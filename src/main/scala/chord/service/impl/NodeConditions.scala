/***************************************************************************
 *                                                                         *
 *                           NodeConditions.scala                          *
 *                            -------------------                          *
 *   date                 : 8.12.2015                                      *
 *   email                : heath.french@utah.edu                          *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   A copy of the license can be found in the license.txt file supplied   *
 *   with this software or at: http://www.gnu.org/copyleft/gpl.html        *
 *                                                                         *
 ***************************************************************************/

package main.scala.chord.service.impl

import main.java.chord.data.ID
import main.java.chord.data.URL
import main.java.chord.com.Entry

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

object NodeConditions{
case class findSuccessor(key: ID)

case class actualNotify(potentialPredecessor: URL)

case class notifyAndCopyEntries(potentialPredecessor: URL)

case class ping()

case class insertEntry(entryToInsert: Entry)

case class insertReplicas(entries: Set[Entry])

case class removeEntry(entryToRemove: Entry)

case class removeReplicas(sendingNode: ID, replicasToRemove: Set[Entry])

case class retrieveEntries(id: ID)

case class leavesNetwork(predecessor: URL)

case class disconnect()
}